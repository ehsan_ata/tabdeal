import Vue from 'vue'
import Vuetify from 'vuetify'
import colors from 'vuetify/es5/util/colors'
Vue.use(Vuetify)

export default new Vuetify({
  theme: {
    light: true,
    themes: {
      light: {
        primary: '#f0b90b',
        secondary: '#111214',
        accent: '#8c9eff',
        error: '#b71c1c',
        coolGrey:'#999a9d',
        lightGrey:'#f6f7fa',
        darken:'#212224'
      },
    },
  }
})
