export { default as NuxtLogo } from '../../components/NuxtLogo.vue'
export { default as Tutorial } from '../../components/Tutorial.vue'
export { default as VuetifyLogo } from '../../components/VuetifyLogo.vue'
export { default as HomeComponentsCurrencyTableComponent } from '../../components/homeComponents/CurrencyTableComponent.vue'
export { default as HomeComponentsFeaturesComponent } from '../../components/homeComponents/FeaturesComponent.vue'
export { default as HomeComponentsIntroComponent } from '../../components/homeComponents/IntroComponent.vue'
export { default as HomeComponentsSliderComponent } from '../../components/homeComponents/SliderComponent.vue'
export { default as HomeComponentsTopSectionComponent } from '../../components/homeComponents/TopSectionComponent.vue'

// nuxt/nuxt.js#8607
function wrapFunctional(options) {
  if (!options || !options.functional) {
    return options
  }

  const propKeys = Array.isArray(options.props) ? options.props : Object.keys(options.props || {})

  return {
    render(h) {
      const attrs = {}
      const props = {}

      for (const key in this.$attrs) {
        if (propKeys.includes(key)) {
          props[key] = this.$attrs[key]
        } else {
          attrs[key] = this.$attrs[key]
        }
      }

      return h(options, {
        on: this.$listeners,
        attrs,
        props,
        scopedSlots: this.$scopedSlots,
      }, this.$slots.default)
    }
  }
}
