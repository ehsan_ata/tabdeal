import Vue from 'vue'
import Router from 'vue-router'
import { normalizeURL, decode } from 'ufo'
import { interopDefault } from './utils'
import scrollBehavior from './router.scrollBehavior.js'

const _91eb8e72 = () => interopDefault(import('../pages/inspire.vue' /* webpackChunkName: "pages/inspire" */))
const _297231d4 = () => interopDefault(import('../pages/login.vue' /* webpackChunkName: "pages/login" */))
const _40257396 = () => interopDefault(import('../pages/markets-overview/index.vue' /* webpackChunkName: "pages/markets-overview/index" */))
const _2e56d206 = () => interopDefault(import('../pages/register.vue' /* webpackChunkName: "pages/register" */))
const _8e996d58 = () => interopDefault(import('../pages/trade/index.vue' /* webpackChunkName: "pages/trade/index" */))
const _219f43ff = () => interopDefault(import('../pages/index.vue' /* webpackChunkName: "pages/index" */))

const emptyFn = () => {}

Vue.use(Router)

export const routerOptions = {
  mode: 'history',
  base: '/',
  linkActiveClass: 'nuxt-link-active',
  linkExactActiveClass: 'nuxt-link-exact-active',
  scrollBehavior,

  routes: [{
    path: "/inspire",
    component: _91eb8e72,
    name: "inspire"
  }, {
    path: "/login",
    component: _297231d4,
    name: "login"
  }, {
    path: "/markets-overview",
    component: _40257396,
    name: "markets-overview"
  }, {
    path: "/register",
    component: _2e56d206,
    name: "register"
  }, {
    path: "/trade",
    component: _8e996d58,
    name: "trade"
  }, {
    path: "/",
    component: _219f43ff,
    name: "index"
  }],

  fallback: false
}

export function createRouter (ssrContext, config) {
  const base = (config._app && config._app.basePath) || routerOptions.base
  const router = new Router({ ...routerOptions, base  })

  // TODO: remove in Nuxt 3
  const originalPush = router.push
  router.push = function push (location, onComplete = emptyFn, onAbort) {
    return originalPush.call(this, location, onComplete, onAbort)
  }

  const resolve = router.resolve.bind(router)
  router.resolve = (to, current, append) => {
    if (typeof to === 'string') {
      to = normalizeURL(to)
    }
    return resolve(to, current, append)
  }

  return router
}
